let skills = {
    knowledge: {
        html: 60,
        css: 50,
        js:20,
        photoshop: 30,
        illustrator: 20,
        adobexd: 50
    },
    languages: {
        english: 70,
        russian: 90,
        french: 40
    }
};